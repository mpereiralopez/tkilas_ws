package com.tkilas.dao;

import java.util.List;
import java.util.Map;
import com.tkilas.model.Local;

public interface LocalDAO {
	

	public Local getLocalById (int localId);
	public List<Map<String,Object>> getNearestLocal(float latitud, float longitud);
	public List<Local> getAllLocalList();
	public List<Map<String,Object>> getNearestLocalByDate(float latitud, float longitud, String specificDate);
}
