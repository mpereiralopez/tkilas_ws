package com.tkilas.dao.impl;


import java.text.SimpleDateFormat;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tkilas.dao.ClientHasDiscountDAO;
import com.tkilas.dao.PackDAO;
import com.tkilas.exceptions.ExceptionSizeLocalLimit;
import com.tkilas.model.ClientDiscountForPOST;
import com.tkilas.model.ClientHasDiscount;
import com.tkilas.model.ClientPromoForPOST;
import com.tkilas.model.Pack;

@Repository
public class ClientHasDiscountDAOImpl implements ClientHasDiscountDAO {

	@Autowired
	private SessionFactory session;
	
	@Autowired
	private PackDAO packDAO;
	
	

	@Override
	public void addDiscountToClient(ClientDiscountForPOST chd) throws ExceptionSizeLocalLimit {
		// TODO Auto-generated method stub
		//System.out.println(chd.getId().getDiscountPackDate().toString());
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
		String dateFormated =  formatoDelTexto.format(chd.getId().getDate());
		Pack pack = packDAO.getPackOfLocalDay(chd.getId().getId_local(), dateFormated);
		
		int restTickets = pack.getSize() - pack.getCounter();
		
		if(restTickets>=chd.getSize()){
			int newCounter = pack.getCounter() + chd.getSize();
			pack.setCounter(newCounter);
			session.getCurrentSession().save(chd);
			packDAO.editPack(pack.getId());
			
		}else{
			throw new ExceptionSizeLocalLimit("ERROR: YA NO QUEDAN TIKETS");
		}
		
	}
	
	@Override
	public void addPromoToClient(ClientPromoForPOST cpr) throws ExceptionSizeLocalLimit {
		// TODO Auto-generated method stub
		//System.out.println(chd.getId().getDiscountPackDate().toString());
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
		String dateFormated =  formatoDelTexto.format(cpr.getId().getDate());
		Pack pack = packDAO.getPackOfLocalDay(cpr.getId().getId_local(), dateFormated);
		
		int restTickets = pack.getSize() - pack.getCounter();
		
		if(restTickets>=cpr.getSize()){
			int newCounter = pack.getCounter() + cpr.getSize();
			pack.setCounter(newCounter);
			session.getCurrentSession().save(cpr);
			packDAO.editPack(pack.getId());
			
		}else{
			throw new ExceptionSizeLocalLimit("ERROR: YA NO QUEDAN TIKETS");
		}
		
	}
	
	@Override
	public List<ClientHasDiscount> getClientsOfDiscountByUserId(int clientId) {
		// TODO Auto-generated method stub
		List<ClientHasDiscount> cHdList = session.getCurrentSession().createQuery("FROM ClientHasDiscount WHERE id_client="+clientId).list();
		return cHdList;
	}

}
