package com.tkilas.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tkilas.dao.LocalDAO;
import com.tkilas.model.Discount;
import com.tkilas.model.Local;
import com.tkilas.model.Pack;
import com.tkilas.model.Promo;

@Repository
public class LocalDAOImp implements LocalDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(LocalDAOImp.class);

	
	@Autowired
	private SessionFactory session;


	@Override
	public Local getLocalById(int localId) {
		// TODO Auto-generated method stub
		logger.debug("LocalDAOimp Buscando Local por ID");

		return (Local)session.getCurrentSession().get(Local.class, localId);
	}
	

	@Override
	public List<Map<String,Object>> getNearestLocal(float latitud, float longitud) {
		// TODO Auto-generated method stub
		logger.debug("####### "+latitud+" "+longitud);
		Query query = session.getCurrentSession().createSQLQuery("CALL geodist("+latitud+","+longitud+",50);");
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<Map<String,Object>> aliasToValueMapList = query.list();
		return aliasToValueMapList;
	}
	
	@Override
	public List<Map<String,Object>> getNearestLocalByDate(float latitud, float longitud, String specificDate) {
		// TODO Auto-generated method stub
		logger.debug("####### "+latitud+" "+longitud);
		Query query = session.getCurrentSession().createSQLQuery("CALL geodist2("+latitud+","+longitud+",'"+specificDate+"',50);");
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<Map<String,Object>> aliasToValueMapList = query.list();
		return aliasToValueMapList;
	}
	
	@Override
	public List<Local> getAllLocalList (){
		String today = "2014-10-21";
		//SELECT Customers.CustomerName, Orders.OrderID FROM Customers LEFT JOIN Orders ON Customers.CustomerID=Orders.CustomerID ORDER BY Customers.CustomerName;
		//String sql = "FROM Local, Pack, Discount, Promo WHERE local.user_id_user = pack.local_user_id_user AND pack.date = '2014-10-15' AND discount.date = '2014-10-15' AND promo.date = '2014-10-15'";
		String sql = "FROM Local l,Pack p INNER JOIN l.userIdUser id WHERE id = p.id.localUserIdUser AND p.id.date = '2014-10-15'" ;
		Query query = session.getCurrentSession().createQuery(sql);
		
		List aliasToValueMapList = query.list();
		
		List<Local> localListFormated = new ArrayList<Local>();
		Local local = null;
		Pack pack = new Pack();
		Discount discount = new Discount();
		Promo promo = new Promo();
		Object [] myObjectArray= new Object [4];
		for(int i = 0; i< aliasToValueMapList.size(); i++){
			myObjectArray = (Object[]) aliasToValueMapList.get(i);
			local = (Local)myObjectArray[0];
			pack = (Pack)myObjectArray[1];
			discount = (Discount)myObjectArray[2];
			promo = (Promo)myObjectArray[3];
			
			localListFormated.add(local);

		}
		
		
		return localListFormated;
	}
	
	
	public List <Local> getLocalByOptionSelectedAndPosition (int option, float latitud, float longitud){
		List <Local> localList = null;	
		List<Map<String,Object>> aliasToValueMapList = getNearestLocal(latitud,longitud);
		switch (option) {
		case 0:
			//BEER
			
			break;
		case 1:
			//CAFE
			break;
			
		case 2:
			//COCKTAIL
			break;
			
		case 3:
			//BOTTLE
			break;
		case 4:
			//POPULAR
			break;
			
		case 5:
			//BEST DIS
			break;
			
			
		default:
			//ALL
			break;
		}
		
		
		
		
		return localList;
		
	}
	
	


}
