package com.tkilas.dao.impl;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tkilas.dao.ClientDAO;
import com.tkilas.model.Client;
import com.tkilas.model.User;

@Repository
public class ClientDAOImpl implements ClientDAO {

	@Autowired
	private SessionFactory session;
	
	private static final Logger logger = LoggerFactory.getLogger(ClientDAOImpl.class);

	
	@Override
	public Client getClientInfoById(int clientId) {
		// TODO Auto-generated method stub
		return (Client)session.getCurrentSession().get(Client.class, clientId);
	}
	
	@Override
	public Client getClientInfoByEmail(String email, String pass) {
		// TODO Auto-generated method stub
		User u = (User)session.getCurrentSession().createQuery("FROM User WHERE email='"+email+"' AND password='"+pass+"'").uniqueResult();
		return getClientInfoById(u.getIdUser());
	}
	
	@Override
	public void addClient(Client client) {
		// TODO Auto-generated method stub
		logger.debug("addClient: CREO UN NUEVO CLIENTE DE MOVIL ");
		session.getCurrentSession().save(client);
	}

}
