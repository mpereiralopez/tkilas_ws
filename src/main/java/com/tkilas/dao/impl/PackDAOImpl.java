package com.tkilas.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tkilas.dao.PackDAO;
import com.tkilas.model.Pack;
import com.tkilas.model.PackPK;
import com.tkilas.model.PacksCanceled;

import org.hibernate.Query;

import com.tkilas.model.Promo;

@Repository
public class PackDAOImpl implements PackDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(PromoDAOImpl.class);
	
	@Autowired
	private SessionFactory session;

	@Override
	public void addPack(Pack pack) {
		// TODO Auto-generated method stub
		logger.debug("PackDAOimp: CREO UN PACK NUEVO ");
		session.getCurrentSession().save(pack);


	}

	@Override
	public void deletePack(PackPK packId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void editPack(PackPK packId) {
		// TODO Auto-generated method stub

	}

	@Override
	public Pack getPack(PackPK packId) {
		// TODO Auto-generated method stub
		logger.debug("PackDAOimp GETPACK Buscando pack: FROM Pack WHERE local_user_id_user="+packId.getLocalUserIdUser()+" AND date="+packId.getDate());		   
		return (Pack) session.getCurrentSession().createQuery("FROM Pack WHERE local_user_id_user="+packId.getLocalUserIdUser()+" AND date ='"+packId.getDate()+"'" ).uniqueResult();
	}

	@Override
	public List<Pack> getPackList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Pack> getPackListOfLocal(int localId, String today) {
		// TODO Auto-generated method stub
		logger.debug("PackDAOimp Buscando pack: FROM Pack WHERE local_user_id_user="+localId+" AND date>"+today);		   
		List<Pack> packList = session.getCurrentSession().createQuery("FROM Pack WHERE local_user_id_user="+localId+" AND date>='"+today+"'").list();
		return packList;
	}

	@Override
	public Pack getPackOfLocalDay(int localId, String today) {
		// TODO Auto-generated method stub
		logger.debug("PackDAOimp Buscando pack: FROM Pack WHERE local_user_id_user="+localId+" AND date="+today);
		Pack pack;
		try{
			pack = (Pack) session.getCurrentSession().createQuery("FROM Pack WHERE local_user_id_user="+localId+" AND date ='"+today+"'" ).uniqueResult();
			
		}catch(java.lang.NullPointerException e){
			logger.debug("PackDAOimp: Exception "+e);
			return null;

		}
		return pack;
	}
	
	@Override
	public Promo getPormoForLocalByDate(int localId, String date){
		logger.debug("getPormoForLocalByDate FROM Pack WHERE local_user_id_user="+localId+" AND date="+date);
		return (Promo)session.getCurrentSession().createQuery("FROM Promo WHERE local_user_id_user="+localId+" AND date ='"+date+"'" ).uniqueResult();
	}

	@Override
	public int updatePromoCounter(int localId, String dateFormated, int size) {
		// TODO Auto-generated method stub
		String sql = "UPDATE Pack set counter = counter+"+size+" where date = '"+dateFormated+"' and local_user_id_user="+localId;
		Query query = session.getCurrentSession().createQuery(sql);
		return query.executeUpdate();
	}

	@Override
	public List<PacksCanceled> getCancelledPacksOfClient(int clientId) {
		// TODO Auto-generated method stub
		return session.getCurrentSession().createQuery("FROM PacksCanceled WHERE id_client="+clientId).list();
	}
	
	
	
}
