package com.tkilas.dao;

import java.util.Date;
import java.util.List;

import com.tkilas.model.Pack;
import com.tkilas.model.PackPK;
import com.tkilas.model.PacksCanceled;
import com.tkilas.model.Promo;

public interface PackDAO {
	
	public void addPack(Pack pack);
	public void deletePack(PackPK packId);
	public void editPack(PackPK packId);
	public Pack getPack(PackPK packId);
	public List<Pack> getPackList ();
	public List<Pack> getPackListOfLocal (int localId,String today);
	public Pack getPackOfLocalDay(int localId, String today);
	public Promo getPormoForLocalByDate(int localId, String date);
	public int updatePromoCounter (int localId, String dateFormated, int size);
	public List<PacksCanceled> getCancelledPacksOfClient(int clientId);
		
}
