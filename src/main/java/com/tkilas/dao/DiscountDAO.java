package com.tkilas.dao;

import java.util.List;

import com.tkilas.model.Discount;

public interface DiscountDAO {
	
	
	public Discount addDiscount(Discount discount);
	
	public Discount getDiscountOfDayForLocal(int localId, String dateFormated);
	
	public List<Discount> getDiscountListForLocal(int localId);

}
