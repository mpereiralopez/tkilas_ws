package com.tkilas.dao;

import java.util.List;

import com.tkilas.model.ClientHasPromo;

public interface ClientHasPromoDAO {
	
	public List<ClientHasPromo> getClientsOfPromoByLocalId(int localId,String today);
	public List<ClientHasPromo> getClientsPromos(int clientId);
}
