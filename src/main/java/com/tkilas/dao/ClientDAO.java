package com.tkilas.dao;

import com.tkilas.model.Client;

public interface ClientDAO {

	public Client getClientInfoById(int clientId);
	public Client getClientInfoByEmail(String email, String pass);

	public void addClient (Client client);
}
