package com.tkilas.dao;

import java.util.List;

import com.tkilas.exceptions.ExceptionSizeLocalLimit;
import com.tkilas.model.ClientDiscountForPOST;
import com.tkilas.model.ClientHasDiscount;
import com.tkilas.model.ClientPromoForPOST;

public interface ClientHasDiscountDAO {
		public void addDiscountToClient(ClientDiscountForPOST chd)throws ExceptionSizeLocalLimit;
		
		public void addPromoToClient(ClientPromoForPOST cp)throws ExceptionSizeLocalLimit;

		public List<ClientHasDiscount> getClientsOfDiscountByUserId(int clientId);

}
