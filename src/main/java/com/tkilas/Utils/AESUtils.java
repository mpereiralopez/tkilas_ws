package com.tkilas.Utils;

import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AESUtils {
	
	private static byte[] generateEncryptionSecret() {
		  try {
		    KeyGenerator generator = KeyGenerator.getInstance("AES");
		    generator.init(128);

		    SecretKey key = generator.generateKey();
		    return key.getEncoded();
		  } catch (NoSuchAlgorithmException ex) {
		    System.out.println("keyutil "+ ex);
		    return null;
		  }		
		}
	 public static byte[] encrypt(String plainText, String encryptionKey) throws Exception {
		    Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding", "SunJCE");
		    SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
		    cipher.init(Cipher.ENCRYPT_MODE, key,new IvParameterSpec(generateEncryptionSecret()));
		    return cipher.doFinal(plainText.getBytes("UTF-8"));
		  }
		 
		  public static String decrypt(byte[] cipherText, String encryptionKey) throws Exception{
		    Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding", "SunJCE");
		    SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
		    cipher.init(Cipher.DECRYPT_MODE, key,new IvParameterSpec(generateEncryptionSecret()));
		    return new String(cipher.doFinal(cipherText),"UTF-8");
		  }

}
