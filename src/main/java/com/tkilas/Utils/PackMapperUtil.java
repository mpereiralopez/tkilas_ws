package com.tkilas.Utils;

import java.util.Date;

public class PackMapperUtil {
	
	private int size;
	private int localId;
	private Date date;
	private Date time_ini;
	private Date time_fin;
	
	private char promo1Type;
	private char promo1Subtype;
	private int promo1_max;
	private double promo1_pvp;
	
	private char promo2Type;
	private char promo2Subtype;
	private int promo2_max;
	private double promo2_pvp;
	
	private int discount;
	
	
	public PackMapperUtil(){}

	public PackMapperUtil(int size, int localId, Date date, Date time_ini,
			Date time_fin, char promo1Type, char promo1Subtype, int promo1_max,
			double promo1_pvp, char promo2Type, char promo2Subtype,
			int promo2_max, double promo2_pvp, int discount) {
		super();
		this.size = size;
		this.localId = localId;
		this.date = date;
		this.time_ini = time_ini;
		this.time_fin = time_fin;
		this.promo1Type = promo1Type;
		this.promo1Subtype = promo1Subtype;
		this.promo1_max = promo1_max;
		this.promo1_pvp = promo1_pvp;
		this.promo2Type = promo2Type;
		this.promo2Subtype = promo2Subtype;
		this.promo2_max = promo2_max;
		this.promo2_pvp = promo2_pvp;
		this.discount = discount;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getLocalId() {
		return localId;
	}

	public void setLocalId(int localId) {
		this.localId = localId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getTime_ini() {
		return time_ini;
	}

	public void setTime_ini(Date time_ini) {
		this.time_ini = time_ini;
	}

	public Date getTime_fin() {
		return time_fin;
	}

	public void setTime_fin(Date time_fin) {
		this.time_fin = time_fin;
	}

	public char getPromo1Type() {
		return promo1Type;
	}

	public void setPromo1Type(char promo1Type) {
		this.promo1Type = promo1Type;
	}

	public char getPromo1Subtype() {
		return promo1Subtype;
	}

	public void setPromo1Subtype(char promo1Subtype) {
		this.promo1Subtype = promo1Subtype;
	}

	public int getPromo1_max() {
		return promo1_max;
	}

	public void setPromo1_max(int promo1_max) {
		this.promo1_max = promo1_max;
	}

	public double getPromo1_pvp() {
		return promo1_pvp;
	}

	public void setPromo1_pvp(double promo1_pvp) {
		this.promo1_pvp = promo1_pvp;
	}

	public char getPromo2Type() {
		return promo2Type;
	}

	public void setPromo2Type(char promo2Type) {
		this.promo2Type = promo2Type;
	}

	public char getPromo2Subtype() {
		return promo2Subtype;
	}

	public void setPromo2Subtype(char promo2Subtype) {
		this.promo2Subtype = promo2Subtype;
	}

	public int getPromo2_max() {
		return promo2_max;
	}

	public void setPromo2_max(int promo2_max) {
		this.promo2_max = promo2_max;
	}

	public double getPromo2_pvp() {
		return promo2_pvp;
	}

	public void setPromo2_pvp(double promo2_pvp) {
		this.promo2_pvp = promo2_pvp;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}
	
	
	

}
