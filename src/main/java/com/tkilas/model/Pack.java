package com.tkilas.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the pack database table.
 * 
 */
@Entity
@Table(name = "pack")

@NamedQuery(name="Pack.findAll", query="SELECT p FROM Pack p")
public class Pack implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PackPK id;

	@Temporal(TemporalType.TIME)
	@Column(name="time_ini")
	private Date time_ini;
	
	@Temporal(TemporalType.TIME)
	@Column(name="time_fin")
	private Date time_fin;
	
	@Column(name="size")
	private int size;
	
	@Column(name="pack_status")
	private int pack_status;
	
	@Column(name="counter")
	private int counter;

	//bi-directional many-to-one association to Local
	@ManyToOne 
	@JoinColumns({
		@JoinColumn(name="local_user_id_user", referencedColumnName="user_id_user",insertable=false, updatable=false),
		})
	private Local local;

	//bi-directional one-to-one association to Discount
	@OneToOne(mappedBy="pack",fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Discount discount;

	//bi-directional one-to-one association to Promo
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "pack", cascade = CascadeType.ALL)
	private List<Promo> promoList;

	public Pack() {
	}

	public PackPK getId() {
		return this.id;
	}

	public void setId(PackPK id) {
		this.id = id;
	}

	public Date getTimeIni() {
		return this.time_ini;
	}

	public void setTimeIni(Date time) {
		this.time_ini = time;
	}
	
	public Date getTimeFin() {
		return this.time_fin;
	}

	public void setTimeFin(Date time) {
		this.time_fin = time;
	}

	public Local getLocal() {
		return this.local;
	}

	public void setLocal(Local local) {
		this.local = local;
	}
	public Discount getDiscount() {
		return this.discount;
	}
	public void setDiscount(Discount discount) {
		this.discount = discount;
	}
	
	public List<Promo> getPromos() {
		return this.promoList;
	}

	public void setPacks(List<Promo> promoList) {
		this.promoList = promoList;
	}
	
	public void setSize(int size){
		this.size = size;
	}
	
	public int getSize(){
		return this.size;
	}

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

	public int getPack_status() {
		return pack_status;
	}

	public void setPack_status(int pack_status) {
		this.pack_status = pack_status;
	}
	
	
	
	

}