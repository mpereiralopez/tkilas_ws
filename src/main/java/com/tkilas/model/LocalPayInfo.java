package com.tkilas.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "localpayinfo")

public class LocalPayInfo implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5249433724453319875L;

	@Id
	//@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_local",insertable=false, updatable=false)
	private int id_local;
	
	@Column(name="trade_name")
	private String trade_name;
	
	@Column(name="contact_name")
	private String contact_name;

	@Column(name="contact_surname")
	private String contact_surname;
	
	@Column(name="contact_address")
	private String contact_address;

	@Column(name="contact_cp")
	private String contact_cp;
	
	@Column(name="contact_city")
	private String contact_city;
	
	@Column(name="contact_IBAN")
	private String contact_IBAN;
	
	
	public LocalPayInfo(){}

	public int getId_local() {
		return id_local;
	}

	public void setId_local(int id_local) {
		this.id_local = id_local;
	}

	public String getTrade_name() {
		return trade_name;
	}

	public void setTrade_name(String trade_name) {
		this.trade_name = trade_name;
	}

	public String getContact_name() {
		return contact_name;
	}

	public void setContact_name(String contact_name) {
		this.contact_name = contact_name;
	}

	public String getContact_surname() {
		return contact_surname;
	}

	public void setContact_surname(String contact_surname) {
		this.contact_surname = contact_surname;
	}

	public String getContact_address() {
		return contact_address;
	}

	public void setContact_address(String contact_address) {
		this.contact_address = contact_address;
	}

	public String getContact_cp() {
		return contact_cp;
	}

	public void setContact_cp(String contact_cp) {
		this.contact_cp = contact_cp;
	}

	public String getContact_city() {
		return contact_city;
	}

	public void setContact_city(String contact_city) {
		this.contact_city = contact_city;
	}

	public String getContact_IBAN() {
		return contact_IBAN;
	}

	public void setContact_IBAN(String contact_IBAN) {
		this.contact_IBAN = contact_IBAN;
	}
	
	
	
	
}
