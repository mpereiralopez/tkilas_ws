package com.tkilas.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "packs_canceled")
public class PacksCanceled {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private int id;
	
	@Column(name="id_local")
	private int id_local;
	
	@Column(name="id_client")
	private int id_client;
	
	@Temporal(TemporalType.DATE)
	@Column(name="date")
	private java.util.Date date;
	
	@Temporal(TemporalType.TIME)
	@Column(name="hour")
	private Date hour;
	
	@Column(name="size")
	private int size;
	
	@Column(name="client_name")
	private String client_name;
	
	@Column(name="client_surname")
	private String client_surname;
	
	@Column(name="status")
	private int status;
	
	@Column(name="discount")
	private int discount;
	
	@Column(name="promo_pvp")
	private float promoPvp;

	@Column(name="promo_size")
	private int promoSize;

	@Column(name="promo_subtype")
	private int promoSubtype;

	@Column(name="promo_type")
	private int promoType;
	
	@Column(name="promo_max_person")
	private int promoMax;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId_local() {
		return id_local;
	}

	public void setId_local(int id_local) {
		this.id_local = id_local;
	}

	public int getId_client() {
		return id_client;
	}

	public void setId_client(int id_client) {
		this.id_client = id_client;
	}

	public java.util.Date getDate() {
		return date;
	}

	public void setDate(java.util.Date date) {
		this.date = date;
	}

	public Date getHour() {
		return hour;
	}

	public void setHour(Date hour) {
		this.hour = hour;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getClient_name() {
		return client_name;
	}

	public void setClient_name(String client_name) {
		this.client_name = client_name;
	}

	public String getClient_surname() {
		return client_surname;
	}

	public void setClient_surname(String client_surname) {
		this.client_surname = client_surname;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public float getPromoPvp() {
		return promoPvp;
	}

	public void setPromoPvp(float promoPvp) {
		this.promoPvp = promoPvp;
	}

	public int getPromoSize() {
		return promoSize;
	}

	public void setPromoSize(int promoSize) {
		this.promoSize = promoSize;
	}

	public int getPromoSubtype() {
		return promoSubtype;
	}

	public void setPromoSubtype(int promoSubtype) {
		this.promoSubtype = promoSubtype;
	}

	public int getPromoType() {
		return promoType;
	}

	public void setPromoType(int promoType) {
		this.promoType = promoType;
	}

	public int getPromoMax() {
		return promoMax;
	}

	public void setPromoMax(int promoMax) {
		this.promoMax = promoMax;
	}
	
	
	
	
}
