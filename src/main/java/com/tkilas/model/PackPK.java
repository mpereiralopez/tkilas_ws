package com.tkilas.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the pack database table.
 * 
 */
@Embeddable
public class PackPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.DATE)
	@Column(name="date", insertable=false, updatable=false)
	private java.util.Date date;

	@Column(name="local_user_id_user", insertable=false, updatable=false)
	private int localUserIdUser;
	
	public PackPK() {
	}
	public java.util.Date getDate() {
		return this.date;
	}
	public void setDate(java.util.Date date) {
		this.date = date;
	}
	public int getLocalUserIdUser() {
		return this.localUserIdUser;
	}
	public void setLocalUserIdUser(int localUserIdUser) {
		this.localUserIdUser = localUserIdUser;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PackPK)) {
			return false;
		}
		PackPK castOther = (PackPK)other;
		return 
			this.date.equals(castOther.date)
			&& (this.localUserIdUser == castOther.localUserIdUser);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.date.hashCode();
		hash = hash * prime + this.localUserIdUser;
		
		return hash;
	}
}