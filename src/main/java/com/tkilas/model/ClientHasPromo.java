package com.tkilas.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the client_has_promo database table.
 * 
 */
@Entity
@Table(name="client_has_promo")
@NamedQuery(name="ClientHasPromo.findAll", query="SELECT c FROM ClientHasPromo c")
public class ClientHasPromo implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ClientHasPromoPK id;

	@Column(name="status")
	private int status;

	@Column(name="size")
	private int reserveSize;
	
	@Column(name="promo_index")
	private int promo_index;

	@Temporal(TemporalType.TIME)
	@Column(name="hour")
	private Date hour;
	
	//bi-directional many-to-one association to Client
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="id_client", referencedColumnName="user_id_user",insertable=false, updatable=false),
		})
	private Client client;

	public ClientHasPromo() {
	}

	public ClientHasPromoPK getId() {
		return this.id;
	}

	public void setId(ClientHasPromoPK id) {
		this.id = id;
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int isAccepted) {
		this.status = isAccepted;
	}

	public int getSize() {
		return this.reserveSize;
	}

	public void setSize(int reserveSize) {
		this.reserveSize = reserveSize;
	}

	public Date getHour() {
		return this.hour;
	}

	public void setHour(Date reservedTime) {
		this.hour = reservedTime;
	}
	
	public Client getClient() {
		return this.client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public int getPromo_index() {
		return promo_index;
	}

	public void setPromo_index(int promo_index) {
		this.promo_index = promo_index;
	}

}