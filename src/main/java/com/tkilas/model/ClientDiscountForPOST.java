package com.tkilas.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "client_has_disscount")
public class ClientDiscountForPOST implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	@EmbeddedId
	private ClientHasDiscountPK id;
	
	@Column(name="size")
	private int size;
	
	@Temporal(TemporalType.TIME)
	@Column(name="hour")
	private Date hour;
	

	public ClientHasDiscountPK getId() {
		return id;
	}
	public void setId(ClientHasDiscountPK id) {
		this.id = id;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	
	public Date getHour() {
		return hour;
	}
	public void setHour(Date hour) {
		this.hour = hour;
	}
	
	

}
