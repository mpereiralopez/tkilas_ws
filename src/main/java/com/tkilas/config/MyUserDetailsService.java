package com.tkilas.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tkilas.dao.UserDAO;
import com.tkilas.model.Role;
import com.tkilas.model.User;

@Service("userDetailsService")
@Transactional(readOnly = true)
public class MyUserDetailsService implements UserDetailsService {

	@Autowired
	private UserDAO dao;

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		User user = dao.getUser(username);
		if (user == null) {
			throw new UsernameNotFoundException("No such user: " + username);
		} else if (user.getRole()==null) {
	        throw new UsernameNotFoundException("User " + username + " has no authorities");
        }
		boolean enabled = true;
		boolean accountNonExpired = true;
		boolean credentialsNonExpired = true;
		boolean accountNonLocked = true;
		
		Set<Role> roles = new HashSet<Role>(0);
		roles.add(user.getRole());
		org.springframework.security.core.userdetails.User u = new org.springframework.security.core.userdetails.User(username, user.getPassword(), enabled, accountNonExpired,credentialsNonExpired, accountNonLocked, getAuthorities(roles));
		return u;
	}
	
	public List<String> getRolesAsList(Set<Role> roles) {
	    List <String> rolesAsList = new ArrayList<String>();
	    for(Role role : roles){
	        rolesAsList.add(role.getName());
	    }
	    return rolesAsList;
	}

	public static List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
	    List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
	    for (String role : roles) {
	        authorities.add(new SimpleGrantedAuthority(role));
	    }
	    return authorities;
	}

	public Collection<? extends GrantedAuthority> getAuthorities(Set<Role> roles) {
	    List<GrantedAuthority> authList = getGrantedAuthorities(getRolesAsList(roles));
	    return authList;
	}
	

}
