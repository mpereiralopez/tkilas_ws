package com.tkilas.service;

import java.util.List;

import com.tkilas.model.ClientHasPromo;

public interface ClientHasPromoService {
	
	public List<ClientHasPromo> getClientsOfPromoByLocalId(int localId,String today);
	public List<ClientHasPromo> getClientsPromos(int clientId) ;

}
