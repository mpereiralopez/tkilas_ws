package com.tkilas.service;

import java.util.List;

import com.tkilas.exceptions.ExceptionSizeLocalLimit;
import com.tkilas.model.ClientDiscountForPOST;
import com.tkilas.model.ClientHasDiscount;
import com.tkilas.model.ClientPromoForPOST;

public interface ClientHasDiscountService {
	
	public void addDiscountToClient(ClientDiscountForPOST clienthdPost) throws ExceptionSizeLocalLimit;
	public void addPromoToClient(ClientPromoForPOST clientPromoPost) throws ExceptionSizeLocalLimit;

	public List<ClientHasDiscount> getClientsOfDiscountByUserId(int clientId);

}
