package com.tkilas.service.imp;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tkilas.dao.PackDAO;
import com.tkilas.model.Pack;
import com.tkilas.model.PacksCanceled;
import com.tkilas.model.Promo;
import com.tkilas.service.PackService;

@Service
public class PackServiceImpl implements PackService {
	
	@Autowired
	private PackDAO packDao;

	@Transactional
	public void addPack(Pack pack) {
		// TODO Auto-generated method stub
		packDao.addPack(pack);

	}

	@Transactional
	public void deletePack(int packId) {
		// TODO Auto-generated method stub

	}

	@Transactional
	public void editPack(int packId) {
		// TODO Auto-generated method stub

	}

	@Transactional
	public Pack getPack(int packId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Transactional
	public List<Pack> getPackList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Transactional
	public List<Pack> getPackListOfLocal(int localId, String today) {
		// TODO Auto-generated method stub
		return packDao.getPackListOfLocal(localId, today);
	}

	@Transactional
	public Pack getPackOfLocalDay(int localId, String today) {
		// TODO Auto-generated method stub
		return packDao.getPackOfLocalDay(localId, today);
	}
	
	@Transactional
	public Promo getPormoForLocalByDate(int localId, String date){
		return packDao.getPormoForLocalByDate(localId, date);
	}
	
	@Transactional
	public List<PacksCanceled> getCancelledPacksOfClient(int clientId){
		return packDao.getCancelledPacksOfClient(clientId);
	}


}
