package com.tkilas.service.imp;


import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tkilas.dao.LocalDAO;
import com.tkilas.model.Local;
import com.tkilas.service.LocalServices;

@Service
public class LocalServicesImpl implements LocalServices {
	
	private static final Logger logger = LoggerFactory.getLogger(LocalServicesImpl.class);
	
	@Autowired
	private LocalDAO localDAO;

	@Transactional
	public Local getLocalById(int localId) {
		// TODO Auto-generated method stub
		return localDAO.getLocalById(localId);
	}

	@Transactional
	public List<Map<String,Object>> getNearestLocal(float latitud, float longitud) {
		// TODO Auto-generated method stub
		return localDAO.getNearestLocal(latitud, longitud);
	}
	
	@Transactional
	public List<Map<String,Object>> getNearestLocalByDate(float latitud, float longitud, String specificDate) {
		// TODO Auto-generated method stub
		return localDAO.getNearestLocalByDate(latitud, longitud, specificDate);
	}
	
	
	@Transactional
	public List<Local> getAllLocalList() {
		// TODO Auto-generated method stub
		return localDAO.getAllLocalList();
	}
	
}
