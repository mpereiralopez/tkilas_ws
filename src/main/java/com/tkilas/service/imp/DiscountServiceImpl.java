package com.tkilas.service.imp;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tkilas.dao.DiscountDAO;
import com.tkilas.model.Discount;
import com.tkilas.service.DiscountService;


@Service
public class DiscountServiceImpl implements DiscountService{
	
	@Autowired
	DiscountDAO discountDAO;
	
	@Transactional
	public Discount addDiscount (Discount discount){
		return discountDAO.addDiscount(discount);
	}

	@Transactional
	public Discount getDiscountOfDayForLocal(int localId, String date) {
		// TODO Auto-generated method stub
		
		return discountDAO.getDiscountOfDayForLocal(localId,date);
	}

	@Transactional
	public List<Discount> getDiscountListForLocal(int localId) {
		// TODO Auto-generated method stub
		return null;
	}

}
