package com.tkilas.service.imp;



import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tkilas.dao.ClientHasDiscountDAO;
import com.tkilas.exceptions.ExceptionSizeLocalLimit;
import com.tkilas.model.ClientDiscountForPOST;
import com.tkilas.model.ClientHasDiscount;
import com.tkilas.model.ClientPromoForPOST;
import com.tkilas.service.ClientHasDiscountService;

@Service
public class ClientHasDiscountServiceImpl implements ClientHasDiscountService {

	@Autowired
	private ClientHasDiscountDAO clientHasDiscountDAO;
	

	@Transactional
	public void addDiscountToClient(ClientDiscountForPOST clienthdPost) throws ExceptionSizeLocalLimit {
		// TODO Auto-generated method stub
		clientHasDiscountDAO.addDiscountToClient(clienthdPost);
		
	}
	
	@Transactional
	public void addPromoToClient(ClientPromoForPOST clientPromoPost) throws ExceptionSizeLocalLimit {
		// TODO Auto-generated method stub
		clientHasDiscountDAO.addPromoToClient(clientPromoPost);
		
	}
	
	@Transactional
	public List<ClientHasDiscount> getClientsOfDiscountByUserId(int clientId) {
		// TODO Auto-generated method stub
		return clientHasDiscountDAO.getClientsOfDiscountByUserId(clientId);
	}

}
