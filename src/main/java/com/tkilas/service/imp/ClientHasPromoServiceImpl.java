package com.tkilas.service.imp;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tkilas.dao.ClientHasPromoDAO;
import com.tkilas.model.ClientHasPromo;
import com.tkilas.service.ClientHasPromoService;

@Service
public class ClientHasPromoServiceImpl implements ClientHasPromoService {

	@Autowired
	private ClientHasPromoDAO clientHasPromoDAO;
	
	
	@Transactional
	public List<ClientHasPromo> getClientsOfPromoByLocalId(int localId,String today) {
		// TODO Auto-generated method stub
		return clientHasPromoDAO.getClientsOfPromoByLocalId(localId, today);
	}
	
	@Transactional
	public List<ClientHasPromo> getClientsPromos(int clientId) {
		return clientHasPromoDAO.getClientsPromos(clientId);
	}



}
