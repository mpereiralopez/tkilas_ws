package com.tkilas.service;

import java.util.List;

import com.tkilas.model.Discount;

public interface DiscountService {

	public Discount addDiscount (Discount discount);
	public Discount getDiscountOfDayForLocal(int localId, String date);
	public List<Discount> getDiscountListForLocal(int localId);

	
}
