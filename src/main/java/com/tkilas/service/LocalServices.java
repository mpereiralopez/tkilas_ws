package com.tkilas.service;

import java.util.List;
import java.util.Map;

import com.tkilas.model.Local;

public interface LocalServices {
	
	public Local getLocalById (int localId);
	public List<Local> getAllLocalList();
	public List<Map<String,Object>> getNearestLocal(float latitud, float longitud);
	
	public List<Map<String,Object>> getNearestLocalByDate(float latitud, float longitud, String specificDate);


}
