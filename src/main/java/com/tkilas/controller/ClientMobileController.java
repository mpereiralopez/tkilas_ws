package com.tkilas.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.tkilas.Utils.ApiKeyUtils;
import com.tkilas.Utils.CustomSqlDateEditor;
import com.tkilas.exceptions.ExceptionSizeLocalLimit;
import com.tkilas.model.Client;
import com.tkilas.model.ClientDiscountForPOST;
import com.tkilas.model.ClientHasDiscount;
import com.tkilas.model.ClientHasPromo;
import com.tkilas.model.ClientPromoForPOST;
import com.tkilas.model.PacksCanceled;
import com.tkilas.model.User;
import com.tkilas.service.ClientHasDiscountService;
import com.tkilas.service.ClientHasPromoService;
import com.tkilas.service.LocalServices;
import com.tkilas.service.PackService;
import com.tkilas.service.UserService;

@Controller
@RequestMapping("/client")
public class ClientMobileController {
	
	@Autowired
	private ClientHasDiscountService cHdService;
	
	@Autowired
	private ClientHasPromoService chPromoService;
	
	@Autowired
	private LocalServices localServices;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private PackService packService;

	private static final Logger logger = LoggerFactory.getLogger(ClientMobileController.class);
	
	@RequestMapping(method=RequestMethod.GET, produces = "application/json" ,value="/get_client_info")
	@ResponseStatus( org.springframework.http.HttpStatus.OK)
	public @ResponseBody String getClientInfo(HttpServletRequest request){	
	    String jsonString = null;

		if(!request.getHeader("API_KEY").equals(ApiKeyUtils.API_KEY)){
			JSONObject errorObj = new JSONObject();
			try {
				errorObj.put("API Error", "NO API KEY");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			jsonString =  errorObj.toString();
		}else{
			
			String username = request.getUserPrincipal().getName();
			logger.debug("############ Nombre de usuario "+username);
			User  u = userService.getUser(username);
			Client c =  userService.getMobileClientByUserId(u.getIdUser());
			
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
			try {
				jsonString = ow.writeValueAsString(c);
			} catch (JsonGenerationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return jsonString; 
			
}
		
	
	@RequestMapping(method = RequestMethod.POST, produces = "application/json", consumes = "application/json", value = "/asociate_client_discount")
	@ResponseStatus(org.springframework.http.HttpStatus.CREATED)
	public @ResponseBody String addClientToADiscount(@RequestBody ClientDiscountForPOST chd, HttpServletRequest request) {
		
		String jsonString = null;
		if (!request.getHeader("API_KEY").equals(ApiKeyUtils.API_KEY)) {
			JSONObject errorObj = new JSONObject();
			try {
				errorObj.put("API Error", "NO API KEY");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			jsonString = errorObj.toString();
		} else {

			System.out.println("/////////////////////////////////////////");
			logger.info("########################### Estoy dentro de metodo addClientToADiscount for client"+ chd.getId().getId_client());
			
			try {
				
			
				//formatoDelTexto.parse(chd.getPk().getDiscountPackDate());
				
				cHdService.addDiscountToClient(chd);

				ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

				jsonString = ow.writeValueAsString(chd);
			} catch (JsonGenerationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				jsonString = e.getMessage();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				jsonString = e.getMessage();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				jsonString = e.getMessage();

			} catch (ExceptionSizeLocalLimit e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				jsonString = e1.getMessage();
			}
		}
		return jsonString;

	}
	
	
	@RequestMapping(method = RequestMethod.POST, produces = "application/json", consumes = "application/json", value = "/asociate_client_promo")
	@ResponseStatus(org.springframework.http.HttpStatus.CREATED)
	public @ResponseBody String addClientToAPromo(@RequestBody ClientPromoForPOST chd, HttpServletRequest request) {
		
		String jsonString = null;
		if (!request.getHeader("API_KEY").equals(ApiKeyUtils.API_KEY)) {
			JSONObject errorObj = new JSONObject();
			try {
				errorObj.put("API Error", "NO API KEY");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			jsonString = errorObj.toString();
		} else {

			System.out.println("/////////////////////////////////////////");
			logger.info("########################### Estoy dentro de metodo addClientToAPromo for client"+ chd.getId().getId_client());
			System.out.println("LocalID: "+chd.getId().getId_client());
			System.out.println("Date: "+chd.getId().getDate());
			System.out.println("PromoIndex: "+chd.getPromo_index());
			System.out.println("PromoSize: "+chd.getSize());
			System.out.println("Id: "+chd.getId());

			try {
				
			
				//formatoDelTexto.parse(chd.getPk().getDiscountPackDate());
				
				cHdService.addPromoToClient(chd);

				ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

				jsonString = ow.writeValueAsString(chd);
			} catch (JsonGenerationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				jsonString = e.getMessage();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				jsonString = e.getMessage();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				jsonString = e.getMessage();

			} catch (ExceptionSizeLocalLimit e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				jsonString = e1.getMessage();
			}
		}
		return jsonString;

	}
	
	
	
	
	@RequestMapping(method=RequestMethod.GET, produces = "application/json" ,value="/packs_user_list/{clientId}")
	@ResponseStatus( org.springframework.http.HttpStatus.OK)
	public @ResponseBody String getListOfPackOfUser(@PathVariable int clientId, HttpServletRequest request){	
		logger.info("Estoy dentro de metodo getListOfDiscountOfUser for userId"+ clientId);
		String jsonString = null;
		if (!request.getHeader("API_KEY").equals(ApiKeyUtils.API_KEY)) {
			JSONObject errorObj = new JSONObject();
			try {
				errorObj.put("API Error", "NO API KEY");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			jsonString = errorObj.toString();
		}else{
			List<ClientHasDiscount> chdList = cHdService.getClientsOfDiscountByUserId(clientId);
			List<ClientHasPromo> chpList = chPromoService.getClientsPromos(clientId);
			List<PacksCanceled> listCanceled =packService.getCancelledPacksOfClient(clientId);
			ObjectMapper mapper = new ObjectMapper();


			try {
				jsonString = "{";
			    jsonString += "\"discounts\":["+mapper.writerWithType(new TypeReference<List<ClientHasDiscount>>() {}).writeValueAsString(chdList)+"],";
				jsonString += "\"promos\":["+mapper.writerWithType(new TypeReference<List<ClientHasPromo>>() {}).writeValueAsString(chpList)+"],";
				jsonString += "\"canceled\":["+mapper.writerWithType(new TypeReference<List<PacksCanceled>>() {}).writeValueAsString(listCanceled)+"]";
				jsonString += "}";

			} catch (JsonGenerationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		    return jsonString;
	}
	
	
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	    SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm:ss aa");
	    dateFormat.setLenient(false);
	    hourFormat.setLenient(false);
	    // true passed to CustomDateEditor constructor means convert empty String to null
	    binder.registerCustomEditor(java.util.Date.class, new CustomDateEditor(hourFormat, true));
	    binder.registerCustomEditor(java.sql.Date.class, new CustomSqlDateEditor(dateFormat));

	}
}
