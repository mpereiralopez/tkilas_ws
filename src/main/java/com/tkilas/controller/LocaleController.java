package com.tkilas.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.tkilas.Utils.ApiKeyUtils;
import com.tkilas.model.Local;
import com.tkilas.service.LocalServices;


@Controller
@RequestMapping("/api/local")
public class LocaleController {
	
	private static final Logger logger = LoggerFactory.getLogger(LocaleController.class);
	
	@Autowired
	private LocalServices localService;
	
	
	@RequestMapping(method=RequestMethod.GET, produces = "application/json" ,value="/get_local_by_id/{localId}")
	public @ResponseBody String get_local_by_id(@PathVariable int localId){
		logger.info("########## Estoy dentro de metodo get_local_by_id"+ localId);
		Local local = localService.getLocalById(localId);
	    ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
	    String jsonString = null;
	    try {
			jsonString = ow.writeValueAsString(local);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return jsonString;
	}
	
	
	
	
	
	
	
	
	@RequestMapping(method=RequestMethod.GET, produces = "application/json;charset=UTF-8" ,value="/get_all_local_list")
	public @ResponseBody String get_all_local_list(HttpServletRequest request){
		logger.info("########## Estoy dentro de metodo get_all_local_list con puntos");
		String jsonString = new String();
		
		 ObjectMapper mapper = new ObjectMapper();

		if(request.getHeaders("API_KEY")!=null && !request.getHeader("API_KEY").equals(ApiKeyUtils.API_KEY)){
			try {
				jsonString =  	new JSONObject().put(HttpStatus.FORBIDDEN.toString(), "Forbidden: No valid API KEY found").toString();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}else{
			List<Local>localList = localService.getAllLocalList();
				try {
					jsonString = mapper.writerWithType(new TypeReference<List<Local>>() {}).writeValueAsString(localList);
				} catch (JsonGenerationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}		    
		}
		
		return jsonString;
	}
	
	
	
	

	/*
	@RequestMapping(method=RequestMethod.GET, produces = "application/json" ,value="/get_offers_of_local/{local_id}")
	public @ResponseBody String get_offers_of_local(ModelMap model, @PathVariable int local_id){
		logger.info("########## Estoy dentro de metodo get_offers_of_local "+ local_id);
		 ObjectMapper mapper = new ObjectMapper();
	     String arrayJsonString = null;
	
	     try {
				arrayJsonString = mapper.writerWithType(new TypeReference<List<Offer>>() {}).writeValueAsString(offerService.getOfferListOfBusiness(local_id));
			} catch (JsonGenerationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  return arrayJsonString;
	}
	
	@RequestMapping(method=RequestMethod.GET, produces = "application/json" ,value="/get_promos_of_local/{local_id}")
	public @ResponseBody String get_promos_of_local(ModelMap model, @PathVariable int local_id){
		logger.info("########## Estoy dentro de metodo get_offers_of_local "+ local_id);
		 ObjectMapper mapper = new ObjectMapper();
	     String arrayJsonString = null;
	
	     try {
				arrayJsonString = mapper.writerWithType(new TypeReference<List<Promos>>() {}).writeValueAsString(promoService.getPromosListOfBusiness(local_id));
			} catch (JsonGenerationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  return arrayJsonString;
	}
	
	
	@RequestMapping(method=RequestMethod.GET, produces = "application/json" ,value="/get_promos_and_offers_of_local/{local_id}")
	public @ResponseBody String get_promos_and_offers_of_local(ModelMap model, @PathVariable int local_id){
		logger.info("########## Estoy dentro de metodo get_promos_and_offers_of_local "+ local_id);
		 ObjectMapper mapper = new ObjectMapper();
	     String arrayJsonString = null;
	     try {
	    	 arrayJsonString ="{\"offers\":"+ mapper.writerWithType(new TypeReference<List<Offer>>() {}).writeValueAsString(offerService.getOfferListOfBusiness(local_id))+",";
	    	 arrayJsonString += "\"promos\": "+ mapper.writerWithType(new TypeReference<List<Promos>>() {}).writeValueAsString(promoService.getPromosListOfBusiness(local_id))+"}";
	    	 
			} catch (JsonGenerationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  return arrayJsonString;
	}
	
	@RequestMapping(method=RequestMethod.POST, produces = "application/json" ,value="/create_new_promo/{local_id}")
	@ResponseStatus( org.springframework.http.HttpStatus.CREATED )
	@ResponseBody public String create_promo( @RequestBody Promos promo, @PathVariable int local_id ){
		promoService.addPromos(promo, local_id );
	    return "Saved promo "+promo.getId().getId();
	   }
	
	@RequestMapping(method=RequestMethod.POST, produces = "application/json" ,value="/create_new_offer/{local_id}")
	@ResponseStatus( org.springframework.http.HttpStatus.CREATED )
	@ResponseBody public String create_offer( @RequestBody Offer offer, @PathVariable int local_id ){
		offerService.addOffer(offer, local_id );
	    return "Saved offer "+offer.getId().getId();
	   }
	
	@RequestMapping(method=RequestMethod.GET, produces = "application/json" ,value="/get_comments_of_local/{local_id}")
	public @ResponseBody String get_comments_of_local(ModelMap model, @PathVariable int local_id){
		logger.info("########## Estoy dentro de metodo get_comments_of_local "+ local_id);
		 ObjectMapper mapper = new ObjectMapper();
	     String arrayJsonString = null;
	     try {
	    	 arrayJsonString = mapper.writerWithType(new TypeReference<List<Comment>>() {}).writeValueAsString(commentService.getCommentListOfBusiness(local_id));	    	 
			} catch (JsonGenerationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  return arrayJsonString;
	}
*/

}
