package com.tkilas.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.tkilas.Utils.ApiKeyUtils;
import com.tkilas.model.Client;
import com.tkilas.model.Local;
import com.tkilas.model.Role;
import com.tkilas.model.User;
import com.tkilas.service.LocalServices;
import com.tkilas.service.UserService;


@Controller
@RequestMapping("/public")
public class PublicAPIController {
	
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private LocalServices localService;
	
	@RequestMapping(method=RequestMethod.POST, value="client/create_new_mobile_client")
	public @ResponseBody String createNewMobileUser(HttpServletRequest request, @RequestParam("clientCity") String clientCity,
			@RequestParam("clientCp") String clientCp,@RequestParam("user_name") String user_name,
			@RequestParam("user_surname") String user_surname,@RequestParam("email") String email,
			@RequestParam("password") String password, @RequestParam("client_SO") int client_SO,
			@RequestParam("clientSex") int clientSex,@RequestParam("deviceUuid") String deviceUuid){	
	    String jsonString = null;
		if(!request.getHeader("API_KEY").equals(ApiKeyUtils.API_KEY)){
			JSONObject errorObj = new JSONObject();
			try {
				errorObj.put("API Error", "NO API KEY");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			jsonString =  errorObj.toString();
		}else{
			User user = new User() ;
			user.setEmail(email);
			user.setPassword(password);
			user.setUser_name(user_name);
			user.setUser_surname(user_surname);
			Role role = new Role();
			role.setIdRole(3);
			role.setName("ROLE_CLIENT");
			user.setRole(role);
			userService.addUser(user);
			
			Client cliente = new Client();
		    cliente.setClient_SO(client_SO);
		    cliente.setClientCity(clientCity);
		    cliente.setClientSex(clientSex);
		    cliente.setClientCp(clientCp);
		    cliente.setDeviceUuid(deviceUuid);
		    cliente.setClientProfilePic("nopic.png");
		    cliente.setUserIdUser(user.getIdUser());
		    cliente.setUser(user);
		    
		    userService.createNewMobileUser(cliente);

		    ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
			try {
				jsonString = ow.writeValueAsString(cliente);
			} catch (JsonGenerationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return jsonString; 
	}
	
	
	
	@RequestMapping(method=RequestMethod.GET, value="client/get_client_by_email")
	public @ResponseBody String findUserMobileByEmail(HttpServletRequest request, @RequestParam("email") String email,
			@RequestParam("pass") String pass){	
	    String jsonString = null;
		if(!request.getHeader("API_KEY").equals(ApiKeyUtils.API_KEY)){
			JSONObject errorObj = new JSONObject();
			try {
				errorObj.put("API Error", "NO API KEY");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			jsonString =  errorObj.toString();
		}else{
			Client cliente = userService.getMobileClientByEmail(email,pass);

		    ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
			try {
				jsonString = ow.writeValueAsString(cliente);
			} catch (JsonGenerationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return jsonString; 
	}
	
	
	
	
	
	@RequestMapping(method=RequestMethod.GET, produces = "application/json;charset=UTF-8" , value="/get_local_by_distance")
	public @ResponseBody String get_nearest_local(HttpServletRequest request){
		float latitudF = Float.parseFloat(request.getParameter("latitud"));
		float longitudF = Float.parseFloat(request.getParameter("longitud"));
		String specificDate = request.getParameter("specificDate");

		logger.info("########## Estoy dentro de metodo get_local_nearest_local con puntos: "+ latitudF+" "+longitudF);
	     String arrayJsonString = null;

		if(request.getHeaders("API_KEY")!=null && !request.getHeader("API_KEY").equals(ApiKeyUtils.API_KEY)){
			try {
				arrayJsonString =  	new JSONObject().put(HttpStatus.FORBIDDEN.toString(), "Forbidden: No valid API KEY found").toString();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}else{
			List<Map<String,Object>>localList = localService.getNearestLocalByDate(latitudF, longitudF, specificDate);
			 ObjectMapper mapper = new ObjectMapper();
		    try {
				arrayJsonString = mapper.writerWithType(new TypeReference<List<Map<String,Object>>>() {}).writeValueAsString(localList);
		    	//arrayJsonString = mapper.writeValueAsString(localList);
			} catch (JsonGenerationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return arrayJsonString;
	}
	
	
	
	

}
