package com.tkilas.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.tkilas.Utils.ApiKeyUtils;
import com.tkilas.model.Client;
import com.tkilas.service.UserService;


@Controller
@RequestMapping("/api/user")
public class UserController {
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	//@Autowired
	//private BusinessService businessService;

	@Autowired
	private UserService userService;

	
	@RequestMapping(method=RequestMethod.GET, value="/users/{userId}")
	public @ResponseBody String findUser(@PathVariable int userId, HttpServletRequest request ){	
	    String jsonString = null;
		if(!request.getHeader("API_KEY").equals(ApiKeyUtils.API_KEY)){
			JSONObject errorObj = new JSONObject();
			try {
				errorObj.put("API Error", "NO API KEY");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			jsonString =  errorObj.toString();
		}else{
			logger.info("Estoy dentro de metodo findUser"+ userId);
		    Client cliente = userService.getMobileClientByUserId(userId);
		    System.out.println(cliente);
		    ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
			try {
				jsonString = ow.writeValueAsString(cliente);
			} catch (JsonGenerationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return jsonString; 
	}
	
	
}
