package com.tkilas.exceptions;

public class ExceptionSizeLocalLimit extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1839283097679066060L;
	
	public ExceptionSizeLocalLimit (String msg){
		super(msg);
	}
	

}
